﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StateSharp
{
    /// <summary>
    /// 表示状态类。
    /// </summary>
    /// <typeparam name="TType">转换类型。</typeparam>
    public class State<TType>
    {
        private IList<Transition<TType>> m_transitions; // 转换实例集合。
        private Func<TType, bool> m_enter;
        /// <summary>
        /// 状态进入方法。
        /// </summary>
        public Func<TType, bool> Enter
        {
            get { return m_enter; }
            set { m_enter = value; }
        }

        private System.Action<TType> m_exit;
        /// <summary>
        /// 状态退出方法。
        /// </summary>
        public System.Action<TType> Exit
        {
            get { return m_exit; }
            set { m_exit = value; }
        }
        private System.Action<TType> m_exec;
        /// <summary>
        /// 状态的执行方法。
        /// </summary>
        public System.Action<TType> Exec
        {
            get { return m_exec; }
            set { m_exec = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public State()
        { 
            m_transitions = new List<Transition<TType>>();
        }
        /// <summary>
        /// 执行动作。
        /// </summary>
        /// <param name="transition"></param>
        /// <returns></returns>
        internal State<TType> DoTransition(TType type)
        {
            foreach (var item in m_transitions)
            {
                var compare = EqualityComparer<TType>.Default;  // 相等表示同一个动作。
                if (compare.Equals(item.Type, type))
                {
                    return item.DestState;
                }
            }
            return null; 
        }
        /// <summary>
        /// 添加转换。
        /// </summary>
        /// <param name="type">转换的类型。</param>
        /// <param name="destState">转换后的状态。</param>
        public void AddTransition(TType type, State<TType> destState)
        {
            Transition<TType> trans = new Transition<TType>(type, destState); 
            m_transitions.Add(trans);
        }
    }
}
