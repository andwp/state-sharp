# StateSharp

#### 介绍  
C#实现的超轻量级状态机。支持.NetFramework 4.0 及以上框架、.NetCore等。

#### 目录说明  

```
├─StateSharp   // StateSharp库代码。
├─TestForm   // 演示项目
└─UnitTest  // 单元测试。
```

#### 使用说明

仅需关注两个类，一个是状态类State<TTransition>负责状态执行、状态装换；另一个是Context<TTransition>类，负责状态调度和管理。  

```
// 初始化代码。
    m_start = new State<Transition>();
    m_pause = new State<Transition>();
    m_close = new State<Transition>();
    m_start.Action = DoStart; 
    m_pause.Action = DoPause;
    m_close.Action = DoClose;
    // 关闭的状态转换。
    m_close.AddTransition(Transition.Start, m_start);
    // 启动状态转换。
    m_start.AddTransition(Transition.Pause, m_pause);
    m_start.AddTransition(Transition.Close, m_close);
    // 暂停状态转换。
    m_pause.AddTransition(Transition.Start, m_start);
    m_pause.AddTransition(Transition.Continue, m_start);
    m_pause.AddTransition(Transition.Close, m_close);
    
    m_context = new Context<Transition>();
    m_context.InitState(m_close);
    m_context.Start(); 

// 调用代码。
    m_context.DoTransition(Transition.Start);
```
示例代码参见[调用演示](https://gitee.com/andwp/state-sharp/blob/master/TestForm/Form1.cs)   
示例代码几乎没有if-else的判断实现了一个计时器功能。演示见下图。   
![avatar](doc/img/demo.gif)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 常见问题  

1. 已经有`stateless` `statemachine` 等C#实现的状态机框架了，为何需要`StateSharp`?    
不可否认，这些框架提供了非常强大的功能，但是它们都太重了。StateSharp可作为动态库使用，也可以直接使用代码文件（只有两个cs文件）。
 